FROM node:current-alpine 

RUN apk add --no-cache --virtual .build-deps python3 g++ make gcc linux-headers udev alsa-lib-dev 

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install

RUN apk add mongodb-tools

RUN apk del .build-deps
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 8080

CMD [ "node", "index.js", "--headless" ]
