const cue_schema =
{
    "setLights":{
        "type":"array",
        "items":{
            "type":"object",
            "properties":{
                "channel":{
                    "isAdaptorContent":false,
                    "type":"integer",
                    "mininum":1,
                    "maximum":512
                },
                "value":{
                    "isAdaptorContent":false,
                    "type":"integer",
                    "mininum":0,
                    "maximum":255
                }
            },
            "required":["channel","value"]
        }
    }
}

let settings_schema = {
    "type":"object",
    "title":"DMX USB Pro Settings",
    "properties":{
      "serialPort":{
        "type":"string",
        "enum":["eins","zwei"]
      },
      "autoConnectOnStart":{ //  would be nicer to just do this via HTML?
          "type": "boolean",
          "default":false
      }
    }
}

const DMX = require("dmx");
try{
    var SerialPort = require('serialport');
}catch(e){
    log.error("dmx-usb", "cannot require serialport. Probably no serialport available on the server running adaptor")
    throw(e) // TODO: throw adaptor error so frontend gets 4xx instead of 500 response
}

let myDMX = new DMX();

async function setup(config, game){ // LIFECYCLE HOOK
    log.info("dmx-usb setup","STARTING ####", config.settings);
    await getSerialPorts();
    
    if (config.settings.autoConnectOnStart){
      connectDMX(config);
    }
    
    log.info("dmx-usb settings schema",settings_schema.plugins);
    return {actions:cue_schema, settings:settings_schema}
}

async function getSerialPorts(){
    await SerialPort.list().then( (ports)=> {
        let portList = [];
        for (const port of ports) {
            //log.info("dmx-usb","possible ports for DMX USB PRO Plugin:", port.comName);    
            portList.push(port.comName)
        };
        settings_schema.properties.serialPort.enum = portList; // populate schema with list of ports. 
        log.info("dmx-usb portList scan",portList)
        return portList
    });
}

function connectDMX(config){
    closeSerial();
    myDMX.addUniverse("one","enttec-usb-dmx-pro",config.settings.serialPort);
    myDMX.update("one", {1:0,2:0,3:0,4:0} ); // initializes black on channels 1-4
    log.info("DMX OBJECT",myDMX.universes.one.dev.close);
}

function setLights(params, session){ // mandatory function because defined above
    
    let output = {};
    for (let setLights of params)
    {
        output[parseInt(setLights.channel)] = setLights.value;
    }
    
    log.info("dmx-usb",output);
    log.info("dmx-usb universe info:",Object.keys(myDMX.universes).length);
    // check if already connected to a universe.
    if (Object.keys(myDMX.universes).length > 0){
        myDMX.update("one",output);
    }
    else
    {
        log.error("dmx-usb", "no serial connection established to DMX Box. no universe set.")
    }

}

async function update(updateData){ // LIFECYCLE HOOK
    log.info("dmx-usb",updateData);
    await getSerialPorts();
}

async function connect(settings){ // LIFECYCLE HOOK
    closeSerial();
    myDMX.addUniverse("one","enttec-usb-dmx-pro",settings.serialPort);
    myDMX.update("one", {1:0,2:0,3:0,4:0} ); // initializes black on channels 1-4*/
}

function disconnect(){ // NOT YET IMPLEMENTED BUT PLANNED
    closeSerial();
}

function closeSerial(){
    if (myDMX && myDMX.universes.one){
        myDMX.universes.one.dev.close(); // TODO: test!
    }
}

module.exports = {
    "setLights":setLights,
    "setup":setup,
    "update":update,
    "connect":connect,
}

/*
Fragen:

- Wie kann ich die Serielle Verbindung trennen?
    - ich hab zwei level mit zwei unterschiedlichen dmx modulen die auf dasselbe gerät zugreifen. Problem: First loaded module gewinnt.
- ich habe jetzt kollisionen zwischen Serial port claims von zwei Plugins. Die können sich um die USB Verbindung streiten oder kommunizieren dass sie nich übernehmen können. Aber wie könnte ich dazu ein gute User Feedback mit Choice anbieten aus einem der Plugins heraus?


Change Request:

- load plugin modules should happen on loading of game and/or even game session not adaptor:ex start
        -- always the case? when to load what? difference in session start and level editor start could be implemented into API
*/